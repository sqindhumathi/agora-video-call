import { createClient, createMicrophoneAndCameraTracks } from "agora-rtc-react";

const appId = "e01b744f5b3044e8b09473e09b823561";
const token =
  "006e01b744f5b3044e8b09473e09b823561IABN7j9/wRrWtfkRfKNWKeE22a8jD9xxDCxgRe8mhBpC8H06wwsAAAAAEAAg4mLWswM/YgEAAQCzAz9i";

export const config = { mode: "rtc", codec: "vp8", appId: appId, token: token };
export const useClient = createClient(config);
export const useMicrophoneAndCameraTracks = createMicrophoneAndCameraTracks();
export const channelName = "Mode-2";
